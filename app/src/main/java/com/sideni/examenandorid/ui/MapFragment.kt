package com.sideni.examenandorid.ui

import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.sideni.examenandorid.R


class MapFragment : Fragment(), OnMapReadyCallback {


    lateinit var map: GoogleMap
    lateinit var fusedLocationClient: FusedLocationProviderClient

    lateinit var mLocationRequest: LocationRequest
    val INTERVAL: Long = 5000

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view =  inflater.inflate(R.layout.activity_main, container, false)



        var mapFragment = childFragmentManager?.findFragmentById(R.id.map) as SupportMapFragment



        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)
        mLocationRequest = LocationRequest()

        return view
    }

    override fun onMapReady(googleMap: GoogleMap?) {


        // objeto que puede o no tener ese objeto, en este caso decimos que tiene algo !!

        map = googleMap!!



        map.uiSettings.isZoomControlsEnabled = true

        map.uiSettings.isMapToolbarEnabled = true
        //map.uiSettings.isMyLocationButtonEnabled = true



        val petStoreLocation = LatLng(19.4976224, -99.1751227)
        val petStoreMarker = MarkerOptions().position(petStoreLocation).title("localizacion")
        map.addMarker(petStoreMarker)

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(petStoreLocation, 16f))

        askForPermission()

    }


    fun askForPermission() {

        if (ActivityCompat.checkSelfPermission(

                this.context!!,

                android.Manifest.permission.ACCESS_FINE_LOCATION

            ) != PackageManager.PERMISSION_GRANTED

        ) {

            requestPermissions(

                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),

                1

            )

            return

        }
        map.isMyLocationEnabled = true

        //fusedLocationClient.lastLocation.addOnSuccessListener {
        //  location ->

        //var userLatLong = LatLng(location.latitude, location.longitude)
        //map.moveCamera(CameraUpdateFactory.newLatLngZoom(userLatLong, 14f))
        //}

        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.setInterval(INTERVAL)

        fusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )


    }
    val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            // do work here
            locationResult.lastLocation
            var locationMessage =
                "LAT: " + locationResult.lastLocation.latitude + " LON: " + locationResult.lastLocation.longitude
    

            Toast.makeText(context, locationMessage, Toast.LENGTH_SHORT).show()
        }
    }

}